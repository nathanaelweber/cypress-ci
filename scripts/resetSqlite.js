const sqlite3 = require('sqlite3').verbose();
const fs = require('fs');
const location = process.env.SQLITE_DB_LOCATION || './todo.db';

db = new sqlite3.Database(location, err => {
  if (err) return rej(err);

  db.run(
    "DELETE FROM todo_items",
    (err, result) => {
        if (err) return rej(err);
        db.close(err => {
          if (err) console.log('error: ' + err)
          else console.log('reset database')
        });
    });
});
